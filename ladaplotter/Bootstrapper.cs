﻿using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ladaplotter.UI;

namespace ladaplotter
{ 
    public class Bootstrapper : BootstrapperBase 
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void Configure()
        {
        }
    }
}