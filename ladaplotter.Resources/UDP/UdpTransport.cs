﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace ladaplotter.Resources.UDP
{
    public class UdpTransport
    {
        private IPEndPoint _localeEndPoint;
        private UdpClient _udpClient;

        public UdpTransport(string ip, int port)
        {
            Ip = ip;
            Port = port;
            _localeEndPoint = new IPEndPoint(IPAddress.Any, Port);
        }

        public string Ip { get; }

        public int Port { get; }

        public bool IsOpen => _udpClient != null;

        public void Open()
        {
            if (_udpClient != null)
                return;

            _udpClient = new UdpClient(_localeEndPoint);
            _udpClient.Client.ReceiveTimeout = 1000;
        }

        public async Task<UdpReceiveResult> ReceiveAsync()
        {
            return await _udpClient.ReceiveAsync();
        }

        public byte[] Receive()
        {
            return _udpClient.Receive(ref _localeEndPoint);
        }

        public void Close()
        {
            _udpClient?.Close();
            _udpClient = null;
        }
    }
}