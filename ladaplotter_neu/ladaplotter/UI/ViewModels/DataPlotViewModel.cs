﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using ladaplotter.Resources.Logic;
using LiveCharts;
using LiveCharts.Wpf;
using ScottPlot;
using ScottPlot.Drawing;

namespace ladaplotter.UI.ViewModels
{
    public class DataPlotViewModel : PropertyChangedBase
    {
        private SeriesCollection _seriesCollection;
        private Plot _signalPlot;

        public DataPlotViewModel()
        {
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Series 1",
                    Values = new ChartValues<double> {4, 6, 5, 2, 4}
                }
            };

            YFormatter = value => value.ToString("N");
        }

        public SeriesCollection SeriesCollection
        {
            get
            {
                return _seriesCollection;
            }
            set
            {
                _seriesCollection = value;
                NotifyOfPropertyChange(()=> SeriesCollection);
            }
        }

        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public async void ChooseFile()
        {
            SignalPlot.Render();
            /*
            var fileDialog = new Microsoft.Win32.OpenFileDialog() { Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*" };
            var result = fileDialog.ShowDialog();
            if (result != false)
            {
                var path = fileDialog.FileName;
                await HeavyMethodAsync(path);
            }*/
        }

        internal async Task HeavyMethodAsync(String in_path)
        {
            var logDataReader = new LogDataReaderFromFile1();
            await logDataReader.Read(in_path);
            var dataList = logDataReader.LogData;

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Series 1",
                    Values = new ChartValues<double>(dataList.Measurements.FirstOrDefault().Values)
                }
            };
        }

        public Plot SignalPlot
        {
            get => _signalPlot;
            set
            {
                _signalPlot = value;
                test();
            }
        }

        private void test()
        {
            var rand = new Random(0);
            double[] values = DataGen.RandomWalk(rand, 100_000);
            int sampleRate = 20_000;

            // Signal plots require a data array and a sample rate (points per unit)
            _signalPlot.Height = 400;
            _signalPlot.AddSignal(values, sampleRate);
            _signalPlot.Benchmark(enable: true);
            _signalPlot.Title($"Signal Plot: One Million Points");
            _signalPlot.Render();
        }
    }
}