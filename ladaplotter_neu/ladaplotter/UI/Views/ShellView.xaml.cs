﻿using System.Windows;
using MahApps.Metro.Controls;

namespace ladaplotter.UI.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : MetroWindow
    {
        public ShellView()
        {
            InitializeComponent();
        }
    }
}
